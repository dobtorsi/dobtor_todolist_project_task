# -*- coding: utf-8 -*-
{
    "name" : "Dobtor Todo List Project Task",
    'version' : '1.0',
    'description' : 'Dobtor todo list project task bridge',
    'author' : "Steven, www.dobtor.com",
    'depends' : ['base','Dobtor_TodoList_Core','project'],
    'data' : [
        'views/dobtor_todolist_project_task.xml',
        'data/subscription_template.xml',
    ],
    'installable' : True,
}